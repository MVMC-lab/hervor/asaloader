# -*- coding: utf-8 -*-

__version__ = "0.5.8"

import asaloader.loader

Loader = asaloader.loader.Loader

__all__ = ['Loader']
