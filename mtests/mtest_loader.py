# -*- coding: utf-8 -*-
import conftest
import serial
import asaloader

ser = serial.Serial()
ser.port = 'COM5'
ser.baudrate = 38400
ser.timeout = 1
ser.open()

loader = asaloader.Loader(ser,
                          device_type=0,
                          is_flash_prog=True,
                          flash_file='test_hexs/m3_hello.hex',
                          is_go_app=True,
                          go_app_delay=3000
                          )

print("device_type", loader.device_type)
print("device_name", loader.device_name)
print("flash_size", loader.flash_size)

for i in range(loader.total_steps):
    loader.do_step()

ser.close()
